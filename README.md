# Test Naves

Just a test of a libgdx minigame using Ashley entity engine. Some code has been copy-pasted from https://github.com/saltares/ashley-superjumper


# Run the example:

Desktop:
./gradlew desktop:run

HTML:
./gradlew html:dist

cd ./html/build/dist ; python -m SimpleHTTPServer ; cd ../../../

Use your favorite browser and point it to http://0.0.0.0:8000
