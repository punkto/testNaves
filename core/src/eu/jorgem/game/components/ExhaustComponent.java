package eu.jorgem.game.components;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import eu.jorgem.game.Defaults;

public class ExhaustComponent implements Component {
	public ParticleEffect pe_left = null;
	public ParticleEffect pe_right = null;
}
