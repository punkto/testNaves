package eu.jorgem.game.systems;

import com.badlogic.ashley.core.*;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.ashley.utils.ImmutableArray;
import com.badlogic.gdx.utils.Array;
import eu.jorgem.game.Defaults;
import eu.jorgem.game.components.CollidableComponent;
import eu.jorgem.game.components.ExplosionComponent;
import eu.jorgem.game.components.TransformComponent;

public class CollidableSystem extends EntitySystem {
	private ComponentMapper<CollidableComponent> cm = ComponentMapper.getFor(CollidableComponent.class);
	private ComponentMapper<TransformComponent> tm = ComponentMapper.getFor(TransformComponent.class);
	private ComponentMapper<ExplosionComponent> em = ComponentMapper.getFor(ExplosionComponent.class);
	private ImmutableArray<Entity> entities;

	public CollidableSystem() {}

	@Override
	public void addedToEngine(Engine engine) {
		entities = engine.getEntitiesFor(Family.all(CollidableComponent.class, TransformComponent.class).get());
	}


	@Override
	public void update(float deltaTime) {
		// System.out.println("Entering MovementSystem's processEntity");
		super.update(deltaTime);

		Entity entityi, entityj;
		TransformComponent transformi, transformj;
		CollidableComponent collidablei, collidablej;
		ExplosionComponent explosioni, explosionj;

		for (int i=0; i<entities.size(); i++) {

			entityi = entities.get(i);
			transformi = tm.get(entityi);
			collidablei = cm.get(entityi);

			for (int j=i+1; j<entities.size(); j++) {
				entityj = entities.get(j);
				transformj = tm.get(entityj);
				collidablej = cm.get(entityj);

				if ((Math.abs(transformi.pos.x - transformj.pos.x) < collidablei.collidable_zone.getWidth())
					&&(Math.abs(transformi.pos.y - transformj.pos.y) < collidablei.collidable_zone.getHeight())) {
					explosioni = em.get(entityi);
					explosionj = em.get(entityj);
					if (explosioni != null) explosioni.destroyed = true;
					if (explosionj != null) explosionj.destroyed = true;
				}



			}

		}

	}
}

