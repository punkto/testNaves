package eu.jorgem;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import eu.jorgem.game.screen.BattleScreen;

public class TestNavesGame extends Game {
	// used by all screens
	public SpriteBatch batcher;

	@Override
	public void create () {
		this.batcher = new SpriteBatch();
		setScreen(new BattleScreen(this));
	}
	
	@Override
	public void render() {
		super.render();
	}
}
