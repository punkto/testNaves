package eu.jorgem.client;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.backends.gwt.GwtApplication;
import com.badlogic.gdx.backends.gwt.GwtApplicationConfiguration;

import eu.jorgem.TestNavesGame;
import eu.jorgem.game.Defaults;

public class HtmlLauncher extends GwtApplication {

	@Override
	public GwtApplicationConfiguration getConfig() {
		return new GwtApplicationConfiguration(Defaults.windowWidth,
				Defaults.windowHeight);
	}

	@Override
	public ApplicationListener createApplicationListener() {
		return new TestNavesGame();
	}
}